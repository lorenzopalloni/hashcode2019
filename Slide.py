from Photo import Photo

class Slide:
    def __init__(self, pictures):
        if len(pictures) > 1 and any(not pic.is_vertical for pic in pictures):
            raise Exception('more than one picture and one is horizontal')
        if len(pictures) > 2:
            raise Exception('more than two pictures are being inserted')

        self.pictures = pictures
    
    def tags(self):
        if len(self.pictures) == 0:
            return set([])
        
        if len(self.pictures) == 1:
            return self.pictures[0].tags

        return self.pictures[0].tags.union(self.pictures[1].tags)
    
    def min_interest(self, other_slide):
        if other_slide is None:
            raise Exception('trying to calculate the interest with None')
        
        my_tags = self.tags()
        other_tags = other_slide.tags()

        common_tags = other_tags.intersection(my_tags)
        only_mine_tags = my_tags - other_tags
        only_other_tags = other_tags - my_tags

        return min(len(common_tags), len(only_mine_tags), len(only_other_tags))

    def __str__(self):
        if len(self.pictures) == 0:
            raise Exception('printing an horizontal slide here')
        
        if len(self.pictures) == 1:
            return str(self.pictures[0].id)
        
        return ' '.join([str(self.pictures[0].id), str(self.pictures[1].id)])
