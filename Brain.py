from random import randint
from Slide import Slide

class Brain:

    def __init__(self, parser):
        self.parser = parser
        self.output_slides = []
        self.slides = []
        self.photos: list = self.parser.list_photos
        self.N = self.parser.N

    def run(self):
        self.think_random()
        self.parser.write_output(self.output_slides)

    def think(self):
        while self.photos:
            self.output_slides.append(self.random_slide())
    
    def think_random(self):
        self.think()
        curr_energy = self.energy()

        length_slideshow = len(self.output_slides)
        
        for i in range(1, min(4, int((length_slideshow - 1 / 2)))):
            curr_energy = self.try_swap(curr_energy, i)
    
    def try_swap(self, curr_energy, swap_size, max_steps=50000):
        length_slideshow = len(self.output_slides)

        for _ in range(max_steps):
            swap_a_start = randint(0, length_slideshow - swap_size)
            swap_a_end = swap_a_start + swap_size - 1
            swap_b_start = randint(0, length_slideshow - swap_size)
            swap_b_end = swap_b_start + swap_size - 1

            if (swap_b_start >= swap_a_start >= swap_b_end) or (swap_a_start >= swap_b_start >= swap_a_end) \
                or (swap_b_start >= swap_a_end >= swap_b_end) or (swap_a_start >= swap_b_end >= swap_a_end):
                continue

            possible_energy = self.move_energy(curr_energy, swap_a_start, swap_a_end, swap_b_start, swap_b_end)
            if possible_energy > curr_energy:
                curr_energy = possible_energy

                for i in range(swap_size):
                    tmp = self.output_slides[swap_a_start + i]
                    self.output_slides[swap_a_start + i] = self.output_slides[swap_b_start + i]
                    self.output_slides[swap_b_start + i] = tmp
        
        return curr_energy
            

    def think_advanced(self):
        while self.photos:
            self.slides.append(self.random_slide2())
        last_slide = self.slides.pop(0)
        self.output_slides.append(last_slide)
        while self.slides:
            best_next_slide = max(self.slides, key=lambda slide: last_slide.min_interest(slide))
            self.slides.remove(best_next_slide)
            last_slide = best_next_slide
            self.output_slides.append(last_slide)

    def random_slide(self):
        photo1 = self.photos.pop(0)
        if photo1.is_vertical:
            i = next(i for i in range(len(self.photos)) if self.photos[i].is_vertical)
            if i is None:
                print('i is none')
            photo2 = self.photos.pop(i)
            slide = Slide([photo1, photo2])
        else:
            slide = Slide([photo1])
        return slide

    def random_slide2(self):
        photo1 = self.photos.pop(randint(0, len(self.photos) - 1))
        if photo1.is_vertical:
            photo2 = max(filter(lambda ph: ph.is_vertical, self.photos), key=lambda ph: len(ph.tags.difference(photo1.tags)))
            self.photos.remove(photo2)
            slide = Slide([photo1, photo2])
        else:
            slide = Slide([photo1])
        return slide


    def energy(self):
        length_slides = len(self.output_slides)
        energy = 0
        for i in range(length_slides -1):
            energy += self.output_slides[i].min_interest(self.output_slides[i + 1])
        
        return energy


    def move_energy(self, curr_energy, swap_a_start, swap_a_end, swap_b_start, swap_b_end):
        energy = curr_energy
        length_slides = len(self.output_slides)

        if swap_a_start > 0:
            energy -= self.output_slides[swap_a_start - 1].min_interest(self.output_slides[swap_a_start])
            energy += self.output_slides[swap_a_start - 1].min_interest(self.output_slides[swap_b_start])
        
        if swap_a_end < length_slides - 1 and (swap_a_end + 1) != swap_b_start:
            energy -= self.output_slides[swap_a_end].min_interest(self.output_slides[swap_a_end + 1])
            energy += self.output_slides[swap_b_end].min_interest(self.output_slides[swap_a_end + 1])
        elif (swap_a_end + 1) == swap_b_start:
            energy -= self.output_slides[swap_a_end].min_interest(self.output_slides[swap_b_start])
            energy += self.output_slides[swap_b_end].min_interest(self.output_slides[swap_a_start])

        if swap_b_start > 0 and (swap_a_end + 1) != swap_b_start:
            energy -= self.output_slides[swap_b_start - 1].min_interest(self.output_slides[swap_b_start])
            energy += self.output_slides[swap_b_start - 1].min_interest(self.output_slides[swap_a_start])
        elif (swap_a_end + 1) == swap_b_start:
            energy -= self.output_slides[swap_b_start - 1].min_interest(self.output_slides[swap_b_start])
            energy += self.output_slides[swap_b_end].min_interest(self.output_slides[swap_a_start])
        
        if swap_b_end < length_slides - 1:
            energy -= self.output_slides[swap_b_end].min_interest(self.output_slides[swap_b_end + 1])
            energy += self.output_slides[swap_a_end].min_interest(self.output_slides[swap_b_end + 1])
        
        return energy