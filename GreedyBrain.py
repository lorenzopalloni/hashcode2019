from random import randint
from Slide import Slide

class Brain:

    def __init__(self, parser):
        self.parser = parser
        self.output_slides = []
        self.slides = []
        self.photos: list = self.parser.list_photos
        self.N = self.parser.N

    def run(self):
        self.think_advanced()
        self.parser.write_output(self.output_slides)

    def think(self):
        while self.photos:
            self.output_slides.append(self.random_slide())

    def think_advanced(self):
        while self.photos:
            self.slides.append(self.random_slide2())
        last_slide = self.slides.pop(0)
        self.output_slides.append(last_slide)
        remaining = len(self.slides)
        while self.slides:
            best_next_slide = max(self.slides, key=lambda slide: last_slide.min_interest(slide))
            self.slides.remove(best_next_slide)
            last_slide = best_next_slide
            self.output_slides.append(last_slide)
            if remaining % 1000 == 0:
                print('remaining: ', remaining)
            remaining -= 1

    def random_slide(self):
        photo1 = self.photos.pop(0)
        if photo1.is_vertical:
            i = next(i for i in range(len(self.photos)) if self.photos[i].is_vertical())
            if i is None:
                print('i is none')
            photo2 = self.photos.pop(i)
            slide = Slide([photo1, photo2])
        else:
            slide = Slide([photo1])
        return slide

    def random_slide2(self):
        photo1 = self.photos.pop(randint(0, len(self.photos) - 1))
        if photo1.is_vertical:
            photo2 = max(filter(lambda ph: ph.is_vertical, self.photos), key=lambda ph: len(ph.tags.difference(photo1.tags)))
            self.photos.remove(photo2)
            slide = Slide([photo1, photo2])
        else:
            slide = Slide([photo1])
        return slide

